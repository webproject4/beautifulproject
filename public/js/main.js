$(document).ready(function () {
   /* Портфолио */
   $('.button[filter]').click(function () {
      if ($(this).attr('val') == 'off') {
         $('.button[filter]').attr('val', 'off').removeClass('focused');
         $(this).attr('val', 'on').addClass('focused');
         $('.filter > div').hide(300);
         $('.filter > div[filter=' + $(this).attr('filter') + ']').show(300);
         if ($(this).attr('filter') == 'all') {
            $('.button[filter]').attr('val', 'off').removeClass('focused');
            $(this).attr('val', 'on').addClass('focused');
            $('.filter > div').show(300);
         }
      }
   });

   /* Форма */
   /*$('#sendN').click(function() {
      var n = document.getElementById('name').value;
      var e = document.getElementById('email').value;
      var t = document.getElementById('text').value;
      if (n == "" || e == "" || t == "") alert('Введите данные.');
      else {
         $(".ajaxForm").submit(function(e){
        e.preventDefault();
        var href = $(this).attr("action");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: href,
            data: $(this).serialize(),
            success: function(response){
                if(response.status == "success"){
                    alert("We received your submission, thank you!");
                }else{
                    alert("An error occured: " + response.message);
                }
            }
        });
    });
      }
   });*/
   $.ajax({
      url: 'https://api.slapform.com/kutori16@mail.ru',
      dataType: 'json',
      method: 'POST',
      data: {
         slap_debug: false,
         slap_captcha: false,
      },
      success: function (response) {
        console.log('Got data: ', response);
        if (response.meta.status == 'success') {
          alert('Submission was successful!');
        } else if (response.meta.status == 'fail') {
          alert('Submission failed with these errors: ', response.meta.errors);
        }
      }
    });
   
});

   var btn = document.querySelectorAll('button');
   
   for (var i = 0; i < btn.length; i++) {
   btn[i].style.outline = 'none';
}
